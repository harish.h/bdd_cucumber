Feature: Trigger Post API 

Scenario Outline: TRigger Post API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in request body 
	When Send the request with data payload 
	Then Validate Post_data_driven status code 
	And Validate Post_data_driven response body parameter
	
Examples:
        |Name|Job|
        |Harish|QA|
        |Anil|SrQA|
        |Arun|Dev|