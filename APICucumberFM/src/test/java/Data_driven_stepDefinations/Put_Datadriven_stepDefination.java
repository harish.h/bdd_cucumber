package Data_driven_stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import API_comman_methods.comman_method_handle_api;
import Endpoint.Put_endpoint;
import Request_repository.Put_request_repository;
import Utility_comman_method.Handle_api_logs;
import Utility_comman_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_Datadriven_stepDefination {
	String requestBody;
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;
	
	@Given("Enter {string} and {string} in Put request body")
	public void enter_and_in_put_request_body(String req_name, String req_job) {
		 log_dir=Handle_directory.create_log_directory("put_tc1_logs");
		 requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		 endpoint = Put_endpoint.put_endpoint_tc1();
	   
	}
	
	
	@When("Send the Put_data_driven with data payload")
	public void send_the_request_with_valid_put_payload() throws IOException {
		 statusCode = comman_method_handle_api.Put_statusCode(requestBody,endpoint);
		 responseBody = comman_method_handle_api.put_responseBody(requestBody,endpoint);
			System.out.println(responseBody);

	}
	@Then("Validate Put_data_driven status code")
	public void validate_the_put_status_code() {
		Assert.assertEquals(statusCode, 200);
	}
	@Then("Validate Put_data_driven response body parameter")
	public void validate_put_response_body_parameter() throws IOException {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);	
		Handle_api_logs.evidence_creator(log_dir,"put_tc1",endpoint,requestBody,responseBody);


	}

}
