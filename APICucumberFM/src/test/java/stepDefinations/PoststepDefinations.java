package stepDefinations;
import static io.restassured.RestAssured.given;

import org.junit.Assert;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class PoststepDefinations {
	String requestBody;
	int statuscode;
	String responsebody;
	
	@Before
	public void setup() {
		System.out.println("\n-------APi Triggered-------");
	}
	@After
	public void teardown() {
		System.out.println("-------Execution Successfull-------");
	}
	
	@Given("Enter NAME and JOB in request body")
	public void enter_name_and_job_in_request_body() {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
	 // throw new io.cucumber.java.PendingException();
	}
	@When("Send the request with payload")
	public void send_the_request_with_payload() {
		statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responsebody = given().header("Content-Type", "application/json").body(requestBody).when()
				.post("/api/users").then().extract().response().asString();
		System.out.println(responsebody);
	//throw io.cucumber.java.PendingException();
	}
	@Then("Validate status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
	  //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate response body parameter")
	public void validate_response_body_parameter() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responsebody);
		String res_name=jspresponse.getString("name");
		String res_job=jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull");
	  //throw new io.cucumber.java.PendingException();
	}



}
