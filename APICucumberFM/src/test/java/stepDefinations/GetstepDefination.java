package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import API_comman_methods.comman_method_handle_api;
import Endpoint.Get_endpoint;
import Utility_comman_method.Handle_api_logs;
import Utility_comman_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class GetstepDefination {
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;
	
	@Given("Valid Get request base url")
	public void valid_get_request_base_url() {
		log_dir=Handle_directory.create_log_directory("get_tc1_logs");
		endpoint = Get_endpoint.Get_endpoint_tc1();
	}
	@When("Send the Get request")
	public void send_the_get_request() throws IOException {
		statusCode = comman_method_handle_api.Get_statusCode(endpoint);
	    responseBody = comman_method_handle_api.get_responseBody(endpoint);
		System.out.println(responseBody);
		
	}
	@Then("Validate the Get status code")
	public void validate_the_get_status_code() {
		Assert.assertEquals(statusCode, 200);
	}
	@Then("Validate Get response body parameter")
	public void validate_get_response_body_parameter() throws IOException {
		JsonPath jsp_res = new JsonPath(responseBody);
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");

		int expected_id[] = { 1, 2, 3, 4, 5, 6 };
		String expected_firstname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String expected_lastname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };
		String expected_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };

		int count = dataarray.length();

		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			int exp_id = expected_id[i];

			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");

			String exp_firstname = expected_firstname[i];
			String exp_lastname = expected_lastname[i];
			String exp_email = expected_email[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_email);
			Handle_api_logs.evidence_creator(log_dir,"get_tc1",endpoint,responseBody,null);
	}

	}
}
