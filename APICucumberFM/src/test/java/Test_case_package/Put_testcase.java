package Test_case_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_comman_methods.comman_method_handle_api;
import Endpoint.Put_endpoint;
import Request_repository.Put_request_repository;
import Utility_comman_method.Handle_api_logs;
import Utility_comman_method.Handle_directory;
import io.restassured.path.json.JsonPath;

public class Put_testcase extends comman_method_handle_api {
	@Test
	public static void executor()  throws IOException { 
		
	 File	log_dir=Handle_directory.create_log_directory("put_tc1_logs");
	 String requestBody = Put_request_repository.put_request_tc1();
				String endpoint = Put_endpoint.put_endpoint_tc1();
				for (int i=0; i<5; i++) {
				
				int statusCode = Put_statusCode(requestBody,endpoint);
				System.out.println(statusCode);
				
				if (statusCode == 200) {
				
			    String responseBody = put_responseBody(requestBody,endpoint);
				System.out.println(responseBody);
				Handle_api_logs.evidence_creator(log_dir,"put_tc1",endpoint,requestBody,responseBody);
				Put_testcase.validator(requestBody,responseBody);
				break;
				}else {
					System.out.println("expected statuscode not found,hence retrying");
				}
				}
		}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);


		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);
	}

	}


