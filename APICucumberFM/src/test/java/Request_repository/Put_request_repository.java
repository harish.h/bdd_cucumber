package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_comman_method.Excel_data_extractor;

public class Put_request_repository { 
	
	public static String put_request_tc1() throws IOException { 
		ArrayList<String> Data=Excel_data_extractor.Excel_data_reader("test_data","put_api","put_tc2");
		String name = Data.get(1);
		String job = Data.get(2);
		
		String requestBody =  "{\r\n" 
				+"\"name\": \""+name+"\",\r\n"
				+ "\"job\": \""+job+"\"\r\n" + "}";
		
		return requestBody;
	}

}
