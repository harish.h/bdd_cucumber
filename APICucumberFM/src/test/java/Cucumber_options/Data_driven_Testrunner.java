package Cucumber_options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src/test/java/Datadriven_features", glue = { "Data_driven_stepDefinations" })

public class Data_driven_Testrunner {

}
