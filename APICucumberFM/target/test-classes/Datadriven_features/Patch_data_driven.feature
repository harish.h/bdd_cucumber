Feature: Trigger Patch API 

Scenario Outline: TRigger the Patch API request with valid  parameters 
	Given Enter "<Name>" and "<Job>" in Patch requestbody 
	When Send the Patch_datadriven with data payload 
	Then Validate Patch_datadriven status code 
	And Validate Patch_datadriven response body parameter
	
Examples:
        |Name|Job|
        |Akash|QA|
        |Adarsh|SrQA|
        |Amar|Dev|