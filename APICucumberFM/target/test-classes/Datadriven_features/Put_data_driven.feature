Feature: Trigger Put API 

Scenario Outline: TRigger Put API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in Put request body 
	When Send the Put_data_driven with data payload 
	Then Validate Put_data_driven status code 
	And Validate Put_data_driven response body parameter
	
Examples:
        |Name|Job|
        |Anand|QA|
        |Sharnu|SrQA|
        |Jaggu|Dev|