Feature: Trigger  API 

@Post
Scenario: TRigger Post API request with valid request parameters 
	Given Enter NAME and JOB in request body 
	When Send the request with payload 
	Then Validate status code 
	And Validate response body parameter 

@Put	
Scenario: TRigger Put API request with valid request parameters 
	Given Input NAME and JOB in Put request body 
	When Send the request with valid put payload 
	Then Validate the Put status code 
	And Validate Put response body parameter 

@Patch	
Scenario: TRigger Patch API request with valid request parameters 
	Given Input NAME and JOB in Patch request body 
	When Send the request with valid Patch payload 
	Then Validate the Patch status code 
	And Validate Patch response body parameter 

@Get	
Scenario: TRigger Get API request with valid request parameters 
	Given Valid Get request base url 
	When Send the Get request 
	Then Validate the Get status code 
	And Validate Get response body parameter 
	
	
	
	
	
	
      